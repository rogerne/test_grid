require "application_system_test_case"

class GridsTest < ApplicationSystemTestCase
  setup do
    @grid = grids(:one)
  end

  test "visiting the index" do
    visit grids_url
    assert_selector "h1", text: "Grids"
  end

  test "creating a Grid" do
    visit grids_url
    click_on "New Grid"

    fill_in "Curr player game", with: @grid.curr_player_game
    fill_in "Curr player name", with: @grid.curr_player_name
    fill_in "Curr player ocean", with: @grid.curr_player_ocean
    fill_in "Direction", with: @grid.direction
    fill_in "Grid x", with: @grid.grid_x
    fill_in "Grid y", with: @grid.grid_y
    fill_in "Opponent gamegrid", with: @grid.opponent_gamegrid
    fill_in "Opponent name", with: @grid.opponent_name
    fill_in "Opponent ocean", with: @grid.opponent_ocean
    click_on "Create Grid"

    assert_text "Grid was successfully created"
    click_on "Back"
  end

  test "updating a Grid" do
    visit grids_url
    click_on "Edit", match: :first

    fill_in "Curr player game", with: @grid.curr_player_game
    fill_in "Curr player name", with: @grid.curr_player_name
    fill_in "Curr player ocean", with: @grid.curr_player_ocean
    fill_in "Direction", with: @grid.direction
    fill_in "Grid x", with: @grid.grid_x
    fill_in "Grid y", with: @grid.grid_y
    fill_in "Opponent gamegrid", with: @grid.opponent_gamegrid
    fill_in "Opponent name", with: @grid.opponent_name
    fill_in "Opponent ocean", with: @grid.opponent_ocean
    click_on "Update Grid"

    assert_text "Grid was successfully updated"
    click_on "Back"
  end

  test "destroying a Grid" do
    visit grids_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Grid was successfully destroyed"
  end
end
