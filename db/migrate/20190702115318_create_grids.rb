class CreateGrids < ActiveRecord::Migration[5.2]
  def change
    create_table :grids do |t|
      t.string :curr_player_name
      t.string :opponent_name
      t.text :curr_player_ocean
      t.text :curr_player_game
      t.text :opponent_ocean
      t.text :opponent_gamegrid
      t.string :direction
      t.string :grid_x
      t.string :grid_y

      t.timestamps
    end
  end
end
