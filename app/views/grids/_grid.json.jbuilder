json.extract! grid, :id, :curr_player_name, :opponent_name, :curr_player_ocean, :curr_player_game, :opponent_ocean, :opponent_gamegrid, :direction, :grid_x, :grid_y, :created_at, :updated_at
json.url grid_url(grid, format: :json)
