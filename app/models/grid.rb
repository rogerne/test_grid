class Grid < ApplicationRecord

  before_save() do
    self.curr_player_ocean = "#{self.grid_x}#{self.grid_y}" unless new_record? 
  end

end
