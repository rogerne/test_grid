Rails.application.routes.draw do
  resources :grids
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'grids#index'
end
